<?php

namespace App\Mail;

use App\Member;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Setting;

class MembersMessageAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $member_id;

    public function __construct($member_id)
    {
        $this->member_id = $member_id;
    }

    public function build()
    {
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject('Website | Membership')
			        ->from($contactEmail)
			        ->view('site/emails/members-message-admin');
    }
}
