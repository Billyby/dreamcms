<?php

namespace App\Mail;

use App\Donation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Setting;

class DonationMessageAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $donation_message;

    public function __construct(Donation $donation_message)
    {
        $this->donation_message = $donation_message;
    }

    public function build()
    {
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject('Website | Donation Form')
			        ->from($contactEmail)
			        ->view('site/emails/donation-message-admin');
    }
}
