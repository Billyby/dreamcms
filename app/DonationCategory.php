<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationCategory extends Model
{
    protected $table = 'donation_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function donations()
    {
        return $this->hasMany(Donations::class, 'category_id');
    }

}
