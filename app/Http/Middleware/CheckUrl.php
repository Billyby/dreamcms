<?php

namespace App\Http\Middleware;

use App\DocumentCategory;
use App\FaqCategory;
use App\GalleryCategory;
use App\News;
use App\NewsCategory;
use App\Page;
use App\PageCategory;
use App\ProductCategory;
use App\Products;
use App\Project;
use App\ProjectCategory;
use App\Property;
use App\PropertyCategory;
use App\SpecialUrl;
use App\TeamCategory;
use App\TeamMember;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CheckUrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->segment(1) == 'dreamcms') {
            return $next($request);
        }

        $dup_request = $request->duplicate();

        $special_url = SpecialUrl::where('url', '=', $request->path())->first();

        if ($special_url) {

            ///documents module
            if ($special_url->module == 'documents') {

                if ($special_url->type == 'category') {

                    $document_category = DocumentCategory::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'documents/' . $document_category->slug);
                }
            }
            ////////

            ///FAQ module
            if ($special_url->module == 'faq') {

                if ($special_url->type == 'category') {

                    $faq_category = FaqCategory::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'faqs/' . $faq_category->slug);
                }
            }
            ////////

            ///Gallery module
            if ($special_url->module == 'gallery') {

                if ($special_url->type == 'category') {

                    $gallery_category = GalleryCategory::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'gallery/' . $gallery_category->slug);
                }
            }
            ////////

            ///news module
            if ($special_url->module == 'news') {

                if ($special_url->type == 'item') {

                    $news = News::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'blog/' . $news->category->slug . '/' . $news->slug);
                }

                if ($special_url->type == 'category') {

                    $news_category = NewsCategory::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'blog/' . $news_category->slug);
                }
            }
            ////////

            ///pages module
            if ($special_url->module == 'pages') {

                if ($special_url->type == 'item') {

                    $page = Page::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'pages/' . $page->category->slug . '/' . $page->slug);
                }

                if ($special_url->type == 'category') {

                    $page_category = PageCategory::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'pages/' . $page_category->slug);
                }
            }
            ////////

            ///products module
            if ($special_url->module == 'products') {

                if ($special_url->type == 'item') {

                    $product = Products::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'products/' . $product->category->slug . '/' . $product->slug);
                }

                if ($special_url->type == 'category') {

                    $product_category = ProductCategory::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'products/' . $product_category->slug);
                }
            }
            ////////

            ///projects module
            if ($special_url->module == 'projects') {

                if ($special_url->type == 'item') {

                    $project = Project::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'projects/' . $project->category->slug . '/' . $project->slug);
                }

                if ($special_url->type == 'category') {

                    $project_category = ProjectCategory::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'projects/' . $project_category->slug);
                }
            }
            ////////

            ///properties module
            if ($special_url->module == 'properties') {

                if ($special_url->type == 'item') {

                    $property = Property::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'properties/' . $property->category->slug . '/' . $property->slug);
                }

                if ($special_url->type == 'category') {

                    $property_category = PropertyCategory::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'properties/' . $property_category->slug);
                }
            }
            ////////

            ///team module
            if ($special_url->module == 'team') {

                if ($special_url->type == 'item') {

                    $team_member = TeamMember::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'team/' . $team_member->category->slug . '/' . $team_member->slug);
                }

                if ($special_url->type == 'category') {

                    $team_category = TeamCategory::where('id', '=', $special_url->item_id)->first();
                    $dup_request->server->set('REQUEST_URI', 'team/' . $team_category->slug);
                }
            }
            ////////

            $dup_request->org_request_path = $request->path();
            $dup_request->is_special_url = true;

            return $next($dup_request);
        }


        $dup_request->is_special_url = false;

        return $next($dup_request);
    }
}
