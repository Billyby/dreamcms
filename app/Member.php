<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Notifications\MemberResetPasswordNotification;

class Member extends Authenticatable //Model
{
    use Sortable;
    use Notifiable;
	
    protected $table = 'members';

    public $sortable = ['firstName', 'lastName', 'email', 'suburb', 'type_id', 'status'];

	
	
	protected $guard = 'member';
	
	protected $fillable = [
            'email', 'password',
        ];

    protected $hidden = [
            'password', 'remember_token',
        ];
	
	
    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function type()
    {
        return $this->belongsTo(MemberType::class, 'type_id');
    }

    public function typesort()
    {
        return $this->hasOne(MemberType::class,'id','type_id');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('members-filter');
        $select = "";

        if($filter['type'] && $filter['type']!="all"){
            $select =  $query->where('type_id', $filter['type']);
        }

        if($filter['search']){
            $select =  $query
				            ->where('firstName','like', '%'.$filter['search'].'%')
				            ->orWhere('lastName','like', '%'.$filter['search'].'%');
				            ;
        }

        return $select;
    }
	
	//Send password reset notification
    public function sendPasswordResetNotification($token)
  {
      $this->notify(new MemberResetPasswordNotification($token));
  }
}
