<!DOCTYPE html>
<html>
<body>
<h1>Donation</h1>
<img src="{{ url('') }}/images/site/email-logo.png">
<br><br>	   
Thank you for your donation!  Here is a summary of your donation to us.<br> 	   
	   
<table class="table">
    @foreach(json_decode($donation_message->data) as $field)
        <tr>
            <th style="width:20%">{{ $field->field }} :</th>
            <td>{{ $field->value }}</td>
        </tr>
    @endforeach
 </table>  
 <br><br>
   
 <table class="table">        
    <tr>
		<th style="width:20%">PAYMENT</th>            
	</tr>
        
	<tr>
		<th style="width:20%">Type :</th>
		<td>{{ $donation_message->payment_type }}</td>
	</tr>
	
	<tr>
		<th style="width:20%">Status :</th>
		<td>{{ $donation_message->payment_status }}</td>
	</tr>
	
	<tr>
		<th style="width:20%">Transaction Number :</th>
		<td>{{ $donation_message->payment_transaction_number }}</td>
	</tr>
	
	<tr>
		<th style="width:20%">Transaction Result :</th>
		<td>{{ $donation_message->payment_transaction_result }}</td>
	</tr>
</table>

<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
