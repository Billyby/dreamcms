@if($header_image)
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="slide" src="{{ $header_image }}" alt="Test">
            <div class="container">
                <div class="carousel-caption text-left">
                </div>
            </div>
        </div>
    </div>
</div>
@endif