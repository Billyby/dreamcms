<div class="col-sm-3 offset-sm-1 blog-sidebar">
    <div class="sidebar-module">
        <h4>News</h4>
        <ol class="navsidebar list-unstyled list-group-flush">
            @if($side_nav_mode=='manual')
                {!! $side_nav !!}
            @endif
            @if($side_nav_mode=='auto')
                @foreach ($side_nav as $item)
                    <li class='list-group-item'><a class="navsidebar" href="{{ url('').'/'.$item->url }}">{{ $item->name }}</a></li>
                @endforeach
            @endif
        </ol>

        <div class='btn-back'>
            <a class='btn-back' href='{{ url('') }}/news/archive'>ARCHIVED NEWS <i class='fa fa-chevron-right'></i></a>
        </div>
    </div>
</div>