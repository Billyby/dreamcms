@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-team')
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post">           
            <h1 class="blog-post-title"></h1>
	              
            @if(count($items))
                  <section class="team-block cards-team">
                     <div class="container">	  
                                                                       
					  @foreach($items as $item)                       								
						<div class='team-list-item'>
							<div class='team-list-item-txt'>
								<h2 class="blog-post-title">{{$item->name}}</h2>
								{!! $item["short_description"] !!}
								<a class='btn btn-lg btn-primary' href='{{ url('').'/'.$item->url }}'>more</a>
							</div>

							@if (count($item->images) > 0)	
								<div class="card border-0 transform-on-hover">	
									<div class='team-list-item-img'>
										<a class="lightbox" href="{{ url('').'/'.$item->url }}">
											<img src="{{ url('') }}{{$item->images[0]->location}}" alt="{{$item->images[0]->name}}" class="card-img-top">
										</a>											
									</div>
								</div>	
							@endif
						</div>																							                                                    
					   @endforeach                                   
                     </div>

                   </section>  
                   <!-- Pagination -->
                   <div id="pagination">{{ $items->links() }}</div>
              
               @else
                 <p>Currently there is no team member to display.</p>
               @endif

          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection
