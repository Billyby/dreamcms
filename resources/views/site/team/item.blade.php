@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/src/baguetteBox.css') }}">
@endsection


<div class="blog-masthead ">
    <div class="container">

        <div class="row">
            @include('site/partials/sidebar-team')

            <div class="col-sm-8 blog-main">
                <section class="team-block cards-team">
                    <div class="container">

                        <div class="blog-post">
                            <h1 class="blog-post-title">{{ $team_member->name }}</h1>

                            <div class='row'>
                                <div class="col-8 row">
                                    <div class="col-12"><strong>Job Title</strong> : {{ $team_member->job_title }}</div>
                                    <div class="col-12"><strong>Role</strong> : {{ $team_member->role }}</div>
                                    <div class="col-12"><strong>Phone</strong> : {{ $team_member->phone }}</div>
                                    <div class="col-12"><strong>Mobile</strong> : {{ $team_member->mobile }}</div>
                                    <div class="col-12"><strong>Email</strong> : {{ $team_member->email }}</div>
                                    <div class="col-12">{!! $team_member->body !!}</div>
                                </div>
                                @if (count($team_member->photo) > 0)
                                    <div class='col-4'>
                                        <div class="card border-0 transform-on-hover">

                                            <a class="lightbox" href="{{ url('') }}{{$team_member->photo}}" data-caption="{{$team_member->name}}">
                                                <img src="{{ url('') }}{{$team_member->photo}}" alt="{{$team_member->name}}" class="card-img-top">
                                            </a>

                                        </div>
                                    </div>
                                @endif

                            </div>

                            <div class='btn-back'>
                                <a class='btn-back' href='{{ url('') }}/team/{{ $team_member->category->slug }}'><i
                                            class='fa fa-chevron-left'></i> back</a>
                            </div>
                        </div>
                    </div>
                </section>

                @include('site/partials/helper-sharing')
            </div><!-- /.blog-post -->
        </div><!-- /.blog-main -->

    </div><!-- /.row -->

</div><!-- /.container -->

@endsection


@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/src/baguetteBox.js') }}"></script>
@endsection


@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            baguetteBox.run('.cards-team', {animation: 'slideIn'});
        });
    </script>
@endsection