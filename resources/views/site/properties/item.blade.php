<?php
// Set Meta Tags
$meta_title_inner = ($property->category->name . " - Properties");
$meta_keywords_inner = "Properties";
$meta_description_inner = ($property->category->name . " - Properties");
?>

@extends('site/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/fotorama/fotorama.css') }}">
@endsection

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">

                <div class="col-sm-12 blog-main">

                    <div class="row">
                        <div class="col-3">

                            <div class="col-12 property-detail-box">

                                @if($property->hide_street_address=='true')
                                {{ $property->suburb }}
                                @else
                                @if($property->unit!='')
                                {{ $property->unit.' / ' }}
                                @endif

                                {{ $property->street_number.' '.$property->street_name }} </br>
                                {{ $property->suburb }}
                                @endif
                                <br/>
                                {{ $property->price_display }}
                                <hr/>

                                @if($property->inspections_data)

                                    <strong>Inspection Times</strong>

                                    @foreach(json_decode($property->inspections_data) as $inspection_date)

                                        <div>
                                            {{ $inspection_date->date }}

                                            {{ $inspection_date->start_time.' - '.$inspection_date->end_time }}
                                        </div>


                                    @endforeach

                                <hr/>
                                @endif

                                @if($property->category->slug=="sale")

                                    @if($property->statement_of_information_pdf)
                                        <a href="{{ $property->statement_of_information_pdf }}" target="_blank">
                                            <i class="far fa-file-pdf"></i> Statement of Information
                                        </a>
                                    @endif
                                    <br />
                                    <a href="/media/due-diligence-checklist.pdf" target="_blank"><i class="far fa-file"></i> Due Diligence Checklist</a>
                                    <hr/>

                                @endif

                                @if($property->leadagent)
                                <div class="agent">
                                    <img src="{{ $property->leadagent->photo }}" class="rounded-circle" alt="{{ $property->leadagent->name }}" width="54" align="left">
                                    <strong>{{ $property->leadagent->name }}</strong><br>
                                    {{ $property->leadagent->mobile }}<br>
                                    {{ $property->leadagent->phone }}<br>
                                    <a href="mailto:{{ $property->leadagent->email }}">{{ $property->leadagent->email }}</a>
                                </div>
                                <hr />
                                @endif

                                @if($property->dualagent)
                                <div class="agent">
                                    <img src="{{ $property->dualagent->photo }}" class="rounded-circle" alt="{{ $property->dualagent->name }}" width="54" align="left">
                                    <strong>{{ $property->dualagent->name }}</strong><br>
                                    {{ $property->dualagent->mobile }}<br>
                                    {{ $property->dualagent->phone }}<br>
                                    <a href="mailto:{{ $property->dualagent->email }}">{{ $property->dualagent->email }}</a>
                                </div>
                                @endif
                            </div>

                        </div>
                        <div class="col-9">

                            <div class="property-header">

                                <div class="row">
                                    <div class="col-7 header-suburb">
                                        {{ $property->suburb }}
                                    </div>
                                    <div class="col-5 header-icons">
                                        {{ $property->bedrooms }} <i class="fas fa-bed mr-1"></i>
                                        {{ $property->bathrooms }} <i class="fas fa-shower mr-1"></i>
                                        {{ $property->garage_spaces }} <i class="fas fa-car"></i>
                                    </div>
                                </div>

                            </div>
                            <div class="fotorama" data-nav="thumbs" data-thumbheight="60" data-width="100%">
                                @foreach($property->images as $image)
                                <img src="{{ $image->location }}">
                                @endforeach
                            </div>

                            <div>{!! $property->description !!}</div>

                            <div id="map">map</div>

                        </div>

                    </div>

                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQFWVXABWZm0M-JiYnNs-6EWFuOpM5woA"></script>
    <script src="{{ asset('/components/fotorama/fotorama.js') }}"></script>
    <script src="{{ asset('/js/site/property.js') }}"></script>
@endsection

@section('inline-scripts')
    <script>

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
        });

        var geocoder= new google.maps.Geocoder();

        geocoder.geocode( { 'address': '1 / 49 Springvale Road Melbourne VIC'}, function(results, status) {
            if (status == 'OK') {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                //alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    </script>
@endsection

