@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Header Images</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-cubes"></i> Header Images</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-body">
                    @if(count($modules))
                        <ul class="nav nav-pills nav-stacked">
                            @foreach($modules as $module)
                                @php
                                $class = ' invisible';
                                if($module->header_image!=null){
                                    $class = '';
                                }
                                @endphp
                                <li>
                                    <h4>{{ $module->display_name }} Module</h4>
                                    <div class="header-image" id="added-image-{{ $module->id }}">
                                        @if($module->header_image!=null)
                                            <image src="{{ $module->header_image }}"/>
                                        @endif
                                    </div>
                                    <div class="header-image-buttons">
                                        <button type="button" data-id="{{ $module->id }}"
                                                class="upload-image btn btn-info btn-sm">Upload Image
                                        </button>
                                        <button id="remove-image-{{ $module->id }}" type="button"
                                                data-id="{{ $module->id }}"
                                                class="remove-image btn btn-danger btn-sm{{ $class }}"
                                                data-toggle=confirmation
                                                data-title="Are you sure?"
                                                data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                                data-btn-cancel-label="No">Remove Image
                                        </button>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        No record
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".upload-image").click(function () {

                var id = $(this).data('id');

                CKFinder.popup({
                    chooseFiles: true,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            var file = evt.data.files.first();
                            $('#added-image-' + id).html('<image src="' + base_url + file.getUrl() + '">');
                            $('#remove-image-' + id).removeClass('invisible');
                            saveImage(id, file.getUrl());
                        });
                        finder.on('file:choose:resizedImage', function (evt) {
                            $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                            $('#remove-image').removeClass('invisible');
                            saveImage(id, evt.data.resizedUrl);
                        });
                    }
                });
            });

            $(".remove-image").click(function () {

                var id = $(this).data('id');
                $('#added-image-' + id).html('');
                $('#remove-image-' + id).addClass('invisible');

                $.ajax({
                    type: "POST",
                    url: '{{ url("dreamcms/settings/remove-header-image") }}',
                    data: {
                        'id': id
                    },
                    success: function (response) {
                        if (response.status == "success") {
                            toastr.options = {"closeButton": true}
                            toastr.success('Header image has been removed');
                        } else {
                            toastr.options = {"closeButton": true}
                            toastr.error('Something Wrong!');
                        }
                    }
                })
            });
        });

        function saveImage(module_id, url) {

            $.ajax({
                type: "POST",
                url: '{{ url("dreamcms/settings/save-header-image") }}',
                data: {
                    'id': module_id,
                    'header_image': url
                },
                success: function (response) {
                    if (response.status == "success") {
                        toastr.options = {"closeButton": true}
                        toastr.success('Header image has been saved');
                    } else {
                        toastr.options = {"closeButton": true}
                        toastr.error('Something Wrong!');
                    }
                }
            });
        }
    </script>
@endsection