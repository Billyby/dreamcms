@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/donations') }}"><i class="fas fa-donate"></i> {{ $display_name }}</a></li>
                <li><a href="{{ url('dreamcms/donations/categories') }}">Categories</a></li>
                <li class="active">Add New</li>
            </ol>
        </section>

        <section class="content">
            <div class="col-sm-12 col-md-10 col-lg-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Category</h3>
                    </div>

                    <form method="post" class="form-horizontal" action="{{ url('dreamcms/donations/store-category') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group {{ ($errors->has('title')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Category Title *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="title" name="title"
                                           placeholder="Category Title" value="{{ old('title') }}">
                                    @if ($errors->has('title'))
                                        <small class="help-block">{{ $errors->first('title') }}</small>
                                    @endif
                                </div>
                            </div>
                                                     
                            <div class="form-group {{ ($errors->has('amount')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Amount $ *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="amount" name="amount"
                                           placeholder="Amount $" value="{{ old('amount') }}">
                                    @if ($errors->has('amount'))
                                        <small class="help-block">{{ $errors->first('amount') }}</small>
                                    @endif
                                </div>
                            </div>                                                     
                                                      
                            @php
                                $status = 'active';
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : null }}>
                                    </label>
                                </div>
                            </div>

                        </div>

                        <div class="box-footer">
                            <a href="{{ url('dreamcms/donations/categories') }}" class="btn btn-info pull-right"
                               data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                               data-btn-cancel-label="No">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">Save
                                & Close
                            </button>
                            <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });            
        });
    </script>
@endsection