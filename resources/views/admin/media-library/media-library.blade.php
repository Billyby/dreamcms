@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper iframe-container">
        <iframe class="cms-iframe" src="{{ url('/') }}/components/ckfinder/ckfinder.html?type=AllFiles&CKEditor=body&CKEditorFuncNum=1&langCode=en-gb"></iframe>
    </div>
@endsection