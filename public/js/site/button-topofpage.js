// JavaScript Document
$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
      $("#btnTopPage").fadeIn(3000);
  } else {
    $("#btnTopPage").fadeOut();
  }
});

$("#btnTopPage").on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});